﻿using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class Track : MonoBehaviour
{
	public List<Waypoint> Waypoints;
	public Profile Profile;
	public float NewWpDist = 10f;
	public bool ConnectMesh = false;

	private Mesh mesh;
	private Vector3[] vertices;
	private int[] triangles;
	private float[] profileLengths01;
	private int p2 = 0;
	private int w2 = 0;

	void OnEnable()
	{
		CreateMesh();
	}

	void OnValidate()  // Must be here, because this is a MonoBehaviour
	{
		if (!Application.isPlaying)
			CreateMesh();
	}

	void Awake()
	{
		if (mesh == null)
		{
			var mf = GetComponent<MeshFilter>();
			mesh = new Mesh();
			mf.mesh = mesh;
			mesh.name = "Track Mesh";
		}
	}

	public void AddWaypoint()
	{
		if (Waypoints == null) { return; }
		var wp = new Waypoint();

		switch (Waypoints.Count)
		{
			case 0:
				wp.Position = Vector3.zero;
				break;
			case 1:
				wp.Position = Waypoints[0].Position + Vector3.forward * NewWpDist;
				break;
			default:
				Vector3 lastPos = Waypoints[Waypoints.Count - 1].Position;
				Vector3 secondToLastPos = Waypoints[Waypoints.Count - 2].Position;
				Vector3 dir = (lastPos - secondToLastPos).normalized;
				wp.Position = lastPos + dir * NewWpDist;
				break;
		}
		wp.Rotation = Quaternion.identity;
		Waypoints.Add(wp);
	}

	public void RemoveLastWaypoint()
	{
		if (Waypoints.Count < 1) { return; }
		Waypoints.RemoveAt(Waypoints.Count - 1);
	}

	public void AddProfilePoint()
	{
		if (Profile.Points == null) { return; }
		List<Vector3> points = Profile.Points;
		Vector3 newPoint = new Vector3();

		switch (points.Count)
		{
			case 0:
				newPoint = Vector3.zero;
				break;
			case 1:
				newPoint = points[0] + Vector3.right;
				break;
			default:
				Vector3 lastPos = points[points.Count - 1];
				Vector3 secondToLastPos = points[points.Count - 2];
				Vector3 dir = (lastPos - secondToLastPos).normalized;
				newPoint = lastPos + dir;
				break;
		}
		points.Add(newPoint);
	}

	public void RemoveLastProfilePoint()
	{
		if (Profile.Points.Count < 1) { return; }
		Profile.Points.RemoveAt(Profile.Points.Count - 1);
	}

	public void ConnectMeshEnds()
	{
		ConnectMesh = true;
	}

	public void CutMeshEnds()
	{
		ConnectMesh = false;
	}

	public void CreateMesh()
	{
		if (mesh == null) { return; }
		mesh.Clear();                       // needed because otherwise the mesh gets messed up when a Profile Point is removed
		SetVertices();
		SetTriangles();
		SetUVs();
		CalculateU();
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		GetComponent<MeshCollider>().sharedMesh = mesh;
	}

	void SetVertices()
	{
		int numOfVerts = Waypoints.Count * Profile.Points.Count;

		if (vertices == null || vertices.Length != numOfVerts)
			vertices = new Vector3[numOfVerts];

		for (int w = 0, v = 0; w < Waypoints.Count; w++)
		{
			Vector3 wpPos = Waypoints[w].Position;
			Quaternion wpRot = Waypoints[w].Rotation;
			for (int p = 0; p < Profile.Points.Count; p++, v++)
			{
				vertices[v] = wpPos + (wpRot * Profile.Points[p]);
			}
		}

		if (mesh == null) { return; }
		mesh.vertices = vertices;
	}

	void SetTriangles()
	{
		int numOfTris;
		if (!ConnectMesh)
			numOfTris = Mathf.Clamp(6 * (Profile.Points.Count - 1) * (Waypoints.Count - 1), 0, int.MaxValue);
		else
			numOfTris = Mathf.Clamp(6 * (Profile.Points.Count - 1) * Waypoints.Count, 0, int.MaxValue);


		if (triangles == null || triangles.Length != numOfTris)
			triangles = new int[numOfTris];

		int ppc = Profile.Points.Count;
		int wc = Waypoints.Count;
		int maxW = ConnectMesh ? wc : wc - 1;
		for (int w = 0, t = 0; w < maxW; w++)
		{
			for (int p = 0; p < ppc - 1; p++, t += 6)
			{
				#region before / no closing meshes
				//int vIdx = w * ppc + p;
				//triangles[t] = vIdx;
				//triangles[t + 1] = triangles[t + 4] = vIdx + ppc;
				//triangles[t + 2] = triangles[t + 3] = vIdx + 1;
				//triangles[t + 5] = vIdx + ppc + 1;
				#endregion

				int wNext = (w + 1) % wc;
				int ll = w * ppc + p;
				int lr = ll + 1;
				int ul = wNext * ppc + p;
				int ur = ul + 1;
				triangles[t] = ll;
				triangles[t + 1] = triangles[t + 3] = ul;
				triangles[t + 2] = triangles[t + 5] = lr;
				triangles[t + 4] = ur;
			}
		}

		if (mesh == null) { return; }
		mesh.triangles = triangles;
	}

	void SetUVs()
	{
		var uvs = new Vector2[vertices.Length];
		int wc = Waypoints.Count;
		int ppc = Profile.Points.Count;
		for (int w = 0; w < wc; w++)
		{
			for (int p = 0; p < ppc; p++)
			{
				//float u = (float)p / (ppc - 1);
				float u = p > 0 ? profileLengths01[p - 1] : 0;
				float v = w;
				int idx = w * ppc + p;
				uvs[idx] = new Vector2(u, v);
			}
		}
		//var uvs = new Vector2[4]
		//{
		//	new Vector3(0,0),
		//	new Vector3(1,0),
		//	new Vector3(0,1),
		//	new Vector3(1,1),
		//};
		mesh.uv = uvs;
	}

	void CalculateU()
	{
		profileLengths01 = new float[Profile.Points.Count - 1];
		float salz = 0f;
		for (int i = 0; i < profileLengths01.Length; i++)
		{
			salz += Vector3.Distance(Profile.Points[i], Profile.Points[i + 1]);
			profileLengths01[i] = salz;
		}
		for (int j = 0; j < profileLengths01.Length; j++)
		{
			profileLengths01[j] = profileLengths01[j] / salz;
		}
	}

	//void OnDrawGizmos()
	//{
	//	Handles.color = Color.cyan;
	//	//for (int i = 0; i < mesh.vertices.Length; i++)
	//	//{
	//	//	Handles.Label(transform.position + mesh.vertices[i], i.ToString());
	//	//}

	//	float m = 1f;
	//	for (int i = 0, t = 0; i < mesh.triangles.Length; i++)
	//	{
	//		Vector3 pos = mesh.vertices[mesh.triangles[i]];
	//		Handles.Label(transform.position + pos + m * Vector3.up * 0.2f, i.ToString());

	//		if (t == 2)
	//		{
	//			t = 0;
	//			m = -m;
	//		}
	//		t++;
	//	}
	//}
}

[System.Serializable]
public class Waypoint
{
	public Interpolation Interpolation;
	public Vector3 Position;
	[HideInInspector]
	public Quaternion Rotation;
}

[System.Serializable]
public class Profile
{
	public List<Vector3> Points;
}

public enum Interpolation { Line, Arc }